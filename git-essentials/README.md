# Git Essentials

---

### Allow Force Push

These commands require your GitLab repository settings to allow force push. To do this, click on your project settings ⇒ repository ⇒ expand branch rules ⇒ view details ⇒ allowed to push and merge / manage in protected branches ⇒ select allowed to force push. You can disable and enable this setting as required.

### [Delete a File From GitLab](https://www.youtube.com/watch?v=Bo-8EfDpKxA)

`git filter-branch --index-filter 'git rm --cached --ignore-unmatch <file_name>' HEAD`

`git push -f`

### Reword a Git Commit

`git rebase -i HEAD~<number_of_commits>`

`git <desired command>`

`<action> (if required, such as when editing a commit message)`

`git push -f`

### .gitignore Project Templates

https://github.com/github/gitignore

---
