<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/WayneBasile/19-week-resources">
    <img src="logo.svg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">19 Week Resources</h3>

  <p align="center">
    Here are some valuable resources, commands, and files I discovered that weren't explicitly included in the curriculum. Feel free to [reach out](https://www.linkedin.com/in/waynebasile/) to me if you have any questions or want to connect!
  </p>
</div>



## Repository Links

* [Git commit guidelines to follow to keep your project organized and stand out to employers.](https://gitlab.com/WayneBasile/19-week-resources/-/blob/main/commit-guidelines/README.md?ref_type=heads)
* [Essential Git commands, including removing a file from the repository, rewording and rebasing commits and messages, and boilerplate .gitignore templates.](https://gitlab.com/WayneBasile/19-week-resources/-/blob/main/git-essentials/README.md?ref_type=heads)
* [A sample professional project README template.](https://gitlab.com/WayneBasile/19-week-resources/-/blob/main/readme-template/README.MD?ref_type=heads)
* [Important notes for Vite and GitLab Pages deployment.](https://gitlab.com/WayneBasile/19-week-resources/-/blob/main/vite-deployment-files/README.md?ref_type=heads)
* [Boilerplate instructions to create a full-stack Vite + React + Django + Docker application.](https://gitlab.com/WayneBasile/19-week-resources/-/blob/main/vite-react-django/README.md?ref_type=heads)
* [You can also visit my boilerplate projects repository, which I plan to continue updating and adding new projects to.](https://gitlab.com/WayneBasile/boilerplate-projects)
